// Copyright (C) 2017 Chris N. Richardson and Garth N. Wells
//
// This file is part of FEniCS Solid Mechanics.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN. If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <pybind11/pybind11.h>
#include <dolfin/log/log.h>

namespace py = pybind11;

namespace fenicssolid_wrappers
{
  void return_mapping(py::module& m);
  void status_update(py::module& m);
  void history_data(py::module& m);
  void plasticity_model(py::module& m);
  void plasticity_problem(py::module& m);
  void quadrature_function(py::module& m);
}


PYBIND11_MODULE(fsm_cpp, m)
{
  // Create module for C++ wrappers
  m.doc() ="FEniCS Solid Mechanics";

  // Create common submodule [common]
  py::module return_mapping = m.def_submodule("return_mapping", "Return Mapping module");
  fenicssolid_wrappers::return_mapping(return_mapping);

  py::module status_update = m.def_submodule("status_update", "Status Update module");
  fenicssolid_wrappers::status_update(status_update);

  py::module history_data = m.def_submodule("history_data", "History Data module");
  fenicssolid_wrappers::history_data(history_data);

  py::module plasticity_model = m.def_submodule("plasticity_model", "Plasticity Model module");
  fenicssolid_wrappers::plasticity_model(plasticity_model);

  py::module plasticity_problem = m.def_submodule("plasticity_problem", "Plasticity Problem module");
  fenicssolid_wrappers::plasticity_problem(plasticity_problem);

  py::module quadrature_function = m.def_submodule("quadrature_function", "Quadrature Function module");
  fenicssolid_wrappers::quadrature_function(quadrature_function);

}
