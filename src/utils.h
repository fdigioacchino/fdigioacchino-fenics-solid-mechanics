// Copyright (C) 2006-2017 Kristian Oelgaard and Garth N. Wells.
//               2018 Fabio Di Gioacchino.
// Licensed under the GNU LGPL Version 3.

#ifndef __FENICS_SOLID_UTILS_H
#define __FENICS_SOLID_UTILS_H

#include <string>
#include <vector>
#include <Eigen/Dense>
#include <boost/multi_array.hpp>

namespace fenicssolid
{

  /// Return git commit hash for library
  std::string git_commit_hash();

  // Compute strain (Voigt notation)
  void compute_deformation_gradient(Eigen::Matrix<double, 3, 3>& F,
                            const boost::multi_array<double, 2>& derivatives,
                            const std::vector<double>& expansion_coeffs);

};
{
  /// Return git commit hash for library
  std::string git_commit_hash();

  void compute_tangent(typedef boost::multi_array<double, 4>& A,
                        const Eigen::Matrix<double, 6, 6>& cons_tangent_voigt, 
                        const Eigen::Matrix<double, 6, 1>& sigma_trial_voigt, 
                        const Eigen::Matrix<double, 3, 3>& Finv, 
                        const double J);

        
    
};


#endif
