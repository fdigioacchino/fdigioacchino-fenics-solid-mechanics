// Copyright (C) 2006-2017 Kristian Oelgaard and Garth N. Wells.
//               2018 Fabio Di Gioacchino.
// Licensed under the GNU LGPL Version 3.
//
// First added:  2009-10-02
// Last changed: 2012-07-17

#include <iostream>

#include <boost/multi_array.hpp>
#include <ufc.h>
#include <ufc_geometry.h>
#include <dolfin/fem/FiniteElement.h>
#include "utils.h"

using namespace fenicssolid;

//-----------------------------------------------------------------------------
std::string fenicssolid::git_commit_hash()
{
  return FENICSSOLID_GIT_COMMIT_HASH;
}
//-----------------------------------------------------------------------------
void fenicssolid::compute_deformation_gradient(Eigen::Matrix<double, 3, 3>& F,
                                       const boost::multi_array<double, 2>& derivatives,
                                       const std::vector<double>& expansion_coeff)
{
  // Identity
  F=Eigen::Matrix<double, 3, 3>::Identity();

  const std::size_t gdim = derivatives.shape()[1];
  const std::size_t space_dim = derivatives.shape()[0];

  for (unsigned int dim = 0; dim < space_dim; dim++)
  {
      F(0,0) += derivatives[dim][0]*expansion_coeff[dim];
    

      F(1,1) += derivatives[dim][1]*expansion_coeff[space_dim + dim];
      
      // F_1,2  (check)
      F(0,1)+= 1*(derivatives[dim][1]*expansion_coeff[dim]);
      
      // F_2,1  (check)
      F(1,0)+= 1*(derivatives[dim][0]*expansion_coeff[space_dim + dim]);
   

    // Add 3D strains
    if (gdim == 3)
    {
        F(2,2)+= derivatives[dim][2]*expansion_coeff[2*space_dim + dim];
        F(0,2)+= derivatives[dim][2]*expansion_coeff[dim];
        F(2,0)+= derivatives[dim][0]*expansion_coeff[2*space_dim + dim];
        F(1,2)+= derivatives[dim][2]*expansion_coeff[space_dim + dim];
        F(2,1)+= derivatives[dim][1]*expansion_coeff[2*space_dim + dim];
          
    }
  }
}
//-----------------------------------------------------------------------------


void fenicssolid::compute_tangent(typedef boost::multi_array<double, 4>& A,
                                  const Eigen::Matrix<double, 6, 6>& cons_tangent_voigt, 
                                  const Eigen::Matrix<double, 6, 1>& sigma_trial_voigt, 
                                  const Eigen::Matrix<double, 3, 3>& Finv, 
                                  const double J)

{        
  
  typedef boost::multi_array<double, 4> fourth_order;
  typedef boost::multi_array<double, 2> second_order;
  typedef fourth_order::index index;
  fourth_order a(boost::extents[3][3][3][3]), c(boost::extents[3][3][3][3]);
  second_order sigma(boost::extents[3][3]) , Id(boost::extents[3][3]), Fi(boost::extents[3][3]);
  
  // Assign values to the element
  Id[0][0]=Id[1][1]=Id[2][2]=1;
  Id[0][1]=Id[1][0]=Id[0][2]=Id[2][0]=Id[1][2]=Id[2][1]=0;
  
  sigma[0][0]= sigma_trial_voigt(0);  sigma[1][1]=sigma_trial_voigt(1);  sigma[2][2]=sigma_trial_voigt(2);
  sigma[0][1]=sigma[1][0]=sigma_trial_voigt(3);
  sigma[1][2]=sigma[2][1]=sigma_trial_voigt(4);
  sigma[0][2]=sigma[2][0]=sigma_trial_voigt(5);
  
  Fi[0][0]= Finv(0,0);  Fi[0][1]= Finv(0,1); Fi[0][2]= Finv(0,2);
  Fi[1][0]= Finv(1,0);  Fi[1][1]= Finv(1,1); Fi[1][2]= Finv(1,2);
  Fi[2][0]= Finv(2,0);  Fi[2][1]= Finv(2,1); Fi[2][2]= Finv(2,2);
  
  c[0][0][0][0] = cons_tangent_voigt(0, 0);
  c[1][1][1][1] = cons_tangent_voigt(1, 1);
  c[2][2][2][2] = cons_tangent_voigt(2, 2);
  c[0][1][0][1] = c[1][0][1][0] = c[1][0][0][1] = c[0][1][1][0] = cons_tangent_voigt(3, 3);
  c[1][2][1][2] = c[2][1][2][1] = c[2][1][1][2] = c[1][2][2][1] = cons_tangent_voigt(4, 4);
  c[0][2][0][2] = c[2][0][2][0] = c[2][0][0][2] = c[0][2][2][0] = cons_tangent_voigt(5, 5);

  c[0][0][1][1] = c[1][1][0][0] = cons_tangent_voigt(0, 1);
  c[0][0][2][2] = c[2][2][0][0] = cons_tangent_voigt(0, 2);
  c[1][1][2][2] = c[2][2][1][1] = cons_tangent_voigt(1, 2);

  c[0][0][0][1] = c[0][1][0][0]= c[1][0][0][0]= c[0][0][1][0] = cons_tangent_voigt(0, 3);
  c[0][0][1][2] = c[1][2][0][0]= c[2][1][0][0]= c[0][0][2][1] = cons_tangent_voigt(0, 4);
  c[0][0][0][2] = c[0][2][0][0]= c[2][0][0][0]= c[0][0][2][0] = cons_tangent_voigt(0, 5);

  c[1][1][0][1] = c[0][1][1][1]= c[1][0][1][1]= c[1][1][1][0] = cons_tangent_voigt(1, 3);
  c[1][1][1][2] = c[1][2][1][1]= c[2][1][1][1]= c[1][1][2][1] = cons_tangent_voigt(1, 4);
  c[1][1][0][2] = c[0][2][1][1]= c[2][0][1][1]= c[1][1][2][0] = cons_tangent_voigt(1, 5);

  c[2][2][0][1] = c[0][1][2][2]= c[1][0][2][2]= c[2][2][1][0] = cons_tangent_voigt(2, 3);
  c[2][2][1][2] = c[1][2][2][2]= c[2][1][2][2]= c[2][2][2][1] = cons_tangent_voigt(2, 4);
  c[2][2][0][2] = c[0][2][2][2]= c[2][0][2][2]= c[2][2][2][0] = cons_tangent_voigt(2, 5);

  c[0][1][1][2] = c[1][2][0][1]= c[2][1][0][1]= c[0][1][2][1] =
  c[1][0][1][2] = c[1][2][1][0]= c[2][1][1][0]= c[1][0][2][1] = cons_tangent_voigt(3, 4);

  c[0][1][0][2] = c[0][2][0][1]= c[2][0][0][1]= c[0][1][2][0] =
  c[1][0][0][2] = c[0][2][1][0]= c[2][0][1][0]= c[1][0][2][0] = cons_tangent_voigt(3, 5); //

  c[1][2][0][2] = c[0][2][1][2]= c[2][0][1][2]= c[1][2][2][0] =
  c[2][1][0][2] = c[0][2][2][1]= c[2][0][2][1]= c[2][1][2][0] = cons_tangent_voigt(4, 5);

  for(index i = 0; i != 3; i++) {
      for (index j = 0; j != 3; j++) {
          for (index k = 0; k != 3; k++) {
              for (index l = 0; l != 3; l++) {
                  for (index m = 0; m != 3; m++) {
                      for (index n = 0; n != 3; n++) {
                           A[i][m][k][n] += J*Fi[m][j] * (c[i][j][k][l] + Id[i][k] * sigma[j][l]) * Fi[n][l];

                      }
                  }
              }
          }
      }
  }


}