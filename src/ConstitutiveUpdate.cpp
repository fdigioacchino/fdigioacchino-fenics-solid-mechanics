// Copyright (C) 2006-2017 Kristian Oelgaard and Garth N. Wells.
//               2018 Fabio Di Gioacchino
// Licensed under the GNU LGPL Version 3.

#include <vector>
#include <ufc.h>
#include <dolfin/fem/FiniteElement.h>
#include <dolfin/fem/GenericDofMap.h>
#include <dolfin/function/Function.h>
#include <dolfin/function/FunctionSpace.h>
#include <dolfin/la/GenericVector.h>
#include <dolfin/mesh/Cell.h>
#include "PlasticityModel.h"
#include "utils.h"
#include "ConstitutiveUpdate.h"

using namespace fenicssolid;

//-----------------------------------------------------------------------------
ConstitutiveUpdate::ConstitutiveUpdate(
  std::shared_ptr<const dolfin::Function> u,
  std::shared_ptr<const dolfin::FiniteElement> sigma_element,
  std::shared_ptr<const dolfin::GenericDofMap> stress_dofmap,
  std::shared_ptr<const PlasticityModel> plastic_model)
  : _gdim(u->function_space()->mesh()->geometry().dim()),
    _u(u),
    _plastic_model(plastic_model),
    _Fp_column(u->function_space()->mesh(), sigma_element, 9),
    _eps_p_equiv(u->function_space()->mesh(), sigma_element, 1),
    _expansion_coeffs(u->function_space()->dofmap()->max_cell_dimension()),
{
  // Get stress UFC element
  auto ufc_element_sigma = sigma_element->ufc_element();
  dolfin_assert(ufc_element_sigma);

  // Get stress dof dimension data
  const std::size_t dim = ufc_element_sigma->space_dimension();
  const std::size_t gdim = ufc_element_sigma->geometric_dimension();
  const std::size_t tdim = ufc_element_sigma->topological_dimension();

  // Get quadrature point coordinates on reference element
  //boost::multi_array<double, 2> ip_coordinates(boost::extents[dim][tdim]);
  _X.resize(boost::extents[dim][tdim]);
  ufc_element_sigma->tabulate_reference_dof_coordinates(_X.data());

  // Get displacement UFC element (single component)
  const dolfin::FiniteElement& u_element_new = *(*_u)[0].function_space()->element();
  auto ufc_element_u = u_element_new.ufc_element();
  dolfin_assert(ufc_element_u);

  // Compute basis function derivatives on reference element and store
  const std::size_t dim_u = ufc_element_u->space_dimension();
  const std::size_t gdim_u = ufc_element_u->geometric_dimension();
  const std::size_t num_points = _X.shape()[0];
  const std::size_t value_size = ufc_element_u->reference_value_size();

  //boost::multi_array<double, 3> derivatives(boost::extents[num_points][dim_u][tdim]);
  this->_derivs_reference.resize(boost::extents[num_points][dim_u][tdim]);
  ufc_element_u->evaluate_reference_basis_derivatives(this->_derivs_reference.data(),
                                                      1, _X.shape()[0], _X.data());

  // Compute number of quadrature points per cell
  const std::size_t num_ip_dofs = sigma_element->value_dimension(0);
  _num_ip_per_cell = sigma_element->space_dimension()/num_ip_dofs;

  // Resize _w_stress/tangent
  const::std::size_t sigma_element_dim = sigma_element->space_dimension();
  _w_stress.resize(num_ip_dofs*_num_ip_per_cell);
  _w_tangent.resize(num_ip_dofs*num_ip_dofs*_num_ip_per_cell);

  const std::size_t num_cells = u->function_space()->mesh()->num_cells();
  _plastic_last.resize(boost::extents[num_cells][_num_ip_per_cell]);
  std::fill(_plastic_last.data(),
            _plastic_last.data() + _plastic_last.num_elements(),
            false);
}
//-----------------------------------------------------------------------------
ConstitutiveUpdate::~ConstitutiveUpdate()
{
  // Do nothing
}
//-----------------------------------------------------------------------------
void ConstitutiveUpdate::update(const dolfin::Cell& cell,
                                const double* vertex_coordinates)
{
  const std::size_t cell_index = cell.index();

  // Get solution dofs on cell
  const auto dofs = _u->function_space()->dofmap()->cell_dofs(cell_index);

  // Get expansion coefficients on cell
  dolfin_assert(_expansion_coeffs.size()
                ==  _u->function_space()->dofmap()->max_cell_dimension());
  _u->vector()->get_local(_expansion_coeffs.data(), dofs.size(), dofs.data());

  Eigen::Matrix<double, 6, 6> cons_tangent_voigt;
  Eigen::Matrix<double, 6, 1> sigma_trial_voigt;
  Eigen::Matrix<double, 3, 3> F, Finv, Piola, sigma_trial;
  Eigen::Matrix<double, 9, 1> Fp_column;
  Eigen::Matrix<double, 1, 1> strain_p_eq;

  typedef boost::multi_array<double, 4> fourth_order;
  typedef fourth_order::index index;
  fourth_order A(boost::extents[3][3][3][3]); //For consistent tangent

  // Call functions on UFC coordinate mapping object
  /*
  void compute_reference_geometry( double * X, double * J, double * detJ, double * K,
                                   std::size_t num_points,
                                   const double * x, const double * coordinate_dofs,
                                   int cell_orientation) const final override
  */

  // Compute geometry mapping
  double J[9];
  double detJ;
  double K[9];
  if (_gdim == 2)
  {
    compute_jacobian_triangle_2d(J, vertex_coordinates);
    compute_jacobian_inverse_triangle_2d(K, detJ, J);
  }
  else if (_gdim == 3)
  {
    compute_jacobian_tetrahedron_3d(J, vertex_coordinates);
    compute_jacobian_inverse_tetrahedron_3d(K, detJ, J);
  }

  // Duplicate data at each point
  auto num_points = _derivs_reference.shape()[0];
  boost::multi_array<double, 2> _J(boost::extents[num_points][9]);
  std::vector<double> _detJ(num_points, detJ);
  boost::multi_array<double, 2> _K(boost::extents[num_points][9]);
  for (std::size_t i = 0; i < num_points; ++i)
  {
    for (std::size_t j = 0; j < 9; ++j)
    {
      _J[i][j] = J[j];
      _K[i][j] = K[j];
    }
  }

  // Get displacement UFC element (single component)
  const dolfin::FiniteElement& u_element_new = *(*_u)[0].function_space()->element();
  auto ufc_element_u = u_element_new.ufc_element();
  dolfin_assert(ufc_element_u);

  // Push derivatives forward to current physical cell
  auto dim_u = _derivs_reference.shape()[1];
  auto gdim = _gdim;
  boost::multi_array<double, 3> derivs_physical(boost::extents[num_points][dim_u][gdim]);
  ufc_element_u->transform_reference_basis_derivatives(derivs_physical.data(), 1, num_points,
                                                       _derivs_reference.data(), _X.data(),
                                                       _J.data(), _detJ.data(), _K.data(), 0);

  // Loop over quadrature points
  for (std::size_t ip = 0; ip < _num_ip_per_cell; ip++)
  {

    // Compute deformation gradient
    compute_deformation_gradient(F, derivs_physical[ip], _expansion_coeffs);

    // Get equivalent plastic strain from previous converged time step
    _eps_p_equiv.get_old_values(cell_index, ip, strain_p_eq);

    // Get Fp from previous converged time step
    _Fp_column.get_old_values(cell_index, ip, Fp_column);

    if (Fp_column(0)==0.0)
    {
      Fp(0,0)=Fp_column(0)+1.0;
      Fp(0,1)=Fp_column(1);
      Fp(0,2)=Fp_column(2);
      Fp(1,0)=Fp_column(3);
      Fp(1,1)=Fp_column(4)+1.0;
      Fp(1,2)=Fp_column(5);
      Fp(2,0)=Fp_column(6);
      Fp(2,1)=Fp_column(7);
      Fp(2,2)=Fp_column(8)+1.0;
    }
    else
    {
      Fp(0,0)=Fp_column(0);
      Fp(0,1)=Fp_column(1);
      Fp(0,2)=Fp_column(2);
      Fp(1,0)=Fp_column(3);
      Fp(1,1)=Fp_column(4);
      Fp(1,2)=Fp_column(5);
      Fp(2,0)=Fp_column(6);
      Fp(2,1)=Fp_column(7);
      Fp(2,2)=Fp_column(8);
    }

    // Testing trial stresses, if yielding occurs the stresses are
    // mapped back onto the yield surface, and the updated parameters
    // are returned.
    const bool active = _plastic_last[cell_index][ip];
    return_mapping.closest_point_projection(_plastic_model, cons_tangent_voigt,
                                            sigma_trial_voigt, F, Fp,
                                            strain_p_eq(0),
                                            active);
    
    _plastic_last[cell_index][ip] = false;


    //Compute tangent (A) and Piola-Kirkoff stress 
    double J=F.determinant();
    Finv= F.inverse();
    compute_tangent(A,cons_tangent_voigt, sigma_trial_voigt, Finv, J);
    
    Piola= J*sigma*Finv.transpose();
    
    // Copy data into structures
    if (_gdim == 3)
    {

      _w_stress[_num_ip_per_cell*0  + ip] = Piola(0,0);
      _w_stress[_num_ip_per_cell*1  + ip] = Piola(1,1);
      _w_stress[_num_ip_per_cell*2  + ip] = Piola(2,2);

      _w_stress[_num_ip_per_cell*3  + ip] = Piola(0,1);
      _w_stress[_num_ip_per_cell*4  + ip] = Piola(1,0);
      _w_stress[_num_ip_per_cell*5  + ip] = Piola(1,2);
      _w_stress[_num_ip_per_cell*6  + ip] = Piola(2,1);
      _w_stress[_num_ip_per_cell*7  + ip] = Piola(0,2);
      _w_stress[_num_ip_per_cell*8  + ip] = Piola(2,0);

      _w_tangent[_num_ip_per_cell*0  + ip] =  A[0][0][0][0];
      _w_tangent[_num_ip_per_cell*1  + ip] =  A[1][1][1][1]; 
      _w_tangent[_num_ip_per_cell*2  + ip] =  A[2][2][2][2];
      _w_tangent[_num_ip_per_cell*3  + ip] =  _w_tangent[_num_ip_per_cell*45  + ip] =  A[0][0][1][1]; //11
      _w_tangent[_num_ip_per_cell*4  + ip] =  _w_tangent[_num_ip_per_cell*46  + ip] =  A[1][1][2][2]; //
      _w_tangent[_num_ip_per_cell*5  + ip] =  _w_tangent[_num_ip_per_cell*47  + ip] =  A[0][0][2][2]; // 
      
      _w_tangent[_num_ip_per_cell*6  + ip] =  _w_tangent[_num_ip_per_cell*48  + ip] = A[0][0][0][1];// = 
      _w_tangent[_num_ip_per_cell*7  + ip] =  _w_tangent[_num_ip_per_cell*49  + ip] = A[0][0][1][0];// =
      _w_tangent[_num_ip_per_cell*8  + ip] =  _w_tangent[_num_ip_per_cell*50  + ip] = A[0][0][1][2];// = 
      _w_tangent[_num_ip_per_cell*9  + ip] =  _w_tangent[_num_ip_per_cell*51  + ip] = A[0][0][2][1];// = 21 
      _w_tangent[_num_ip_per_cell*10  + ip] = _w_tangent[_num_ip_per_cell*52  + ip] = A[0][0][0][2];// =
      _w_tangent[_num_ip_per_cell*11  + ip] = _w_tangent[_num_ip_per_cell*53  + ip] = A[0][0][2][0];// 
      _w_tangent[_num_ip_per_cell*12 + ip] =  _w_tangent[_num_ip_per_cell*54  + ip] = A[1][1][0][1];// = 
      _w_tangent[_num_ip_per_cell*13  + ip] = _w_tangent[_num_ip_per_cell*55  + ip] = A[1][1][1][0];// =
      _w_tangent[_num_ip_per_cell*14  + ip] = _w_tangent[_num_ip_per_cell*56  + ip] = A[1][1][1][2]; // 
      _w_tangent[_num_ip_per_cell*15  + ip] = _w_tangent[_num_ip_per_cell*57  + ip] = A[1][1][2][1];// =
      _w_tangent[_num_ip_per_cell*16  + ip] = _w_tangent[_num_ip_per_cell*58  + ip] = A[1][1][0][2];
      _w_tangent[_num_ip_per_cell*17  + ip] = _w_tangent[_num_ip_per_cell*59  + ip] = A[1][1][2][0]; // =
      _w_tangent[_num_ip_per_cell*18 + ip] =  _w_tangent[_num_ip_per_cell*60  + ip] = A[2][2][0][1];
      _w_tangent[_num_ip_per_cell*19  + ip] = _w_tangent[_num_ip_per_cell*61  + ip] = A[2][2][1][0];
      _w_tangent[_num_ip_per_cell*20  + ip] = _w_tangent[_num_ip_per_cell*62  + ip] = A[2][2][1][2]; 
      _w_tangent[_num_ip_per_cell*21  + ip] = _w_tangent[_num_ip_per_cell*63  + ip] = A[2][2][2][1];
      _w_tangent[_num_ip_per_cell*22  + ip] = _w_tangent[_num_ip_per_cell*64  + ip] = A[2][2][0][2];
      _w_tangent[_num_ip_per_cell*23  + ip] = _w_tangent[_num_ip_per_cell*65  + ip] = A[2][2][2][0]; 
      
      _w_tangent[_num_ip_per_cell*24 + ip] =  A[0][1][0][1];
      _w_tangent[_num_ip_per_cell*25  + ip] = _w_tangent[_num_ip_per_cell*66  + ip] = A[0][1][1][0];
      _w_tangent[_num_ip_per_cell*26  + ip] = _w_tangent[_num_ip_per_cell*67  + ip] = A[0][1][1][2]; 
      _w_tangent[_num_ip_per_cell*27  + ip] = _w_tangent[_num_ip_per_cell*68  + ip] = A[0][1][2][1];
      _w_tangent[_num_ip_per_cell*28  + ip] = _w_tangent[_num_ip_per_cell*69  + ip] = A[0][1][0][2];
      _w_tangent[_num_ip_per_cell*29  + ip] = _w_tangent[_num_ip_per_cell*70  + ip] = A[0][1][2][0]; 
      
      _w_tangent[_num_ip_per_cell*30  + ip] = A[1][0][1][0];
      _w_tangent[_num_ip_per_cell*31  + ip] = _w_tangent[_num_ip_per_cell*71  + ip] = A[1][0][1][2]; 
      _w_tangent[_num_ip_per_cell*32  + ip] = _w_tangent[_num_ip_per_cell*72  + ip] = A[1][0][2][1];
      _w_tangent[_num_ip_per_cell*33  + ip] = _w_tangent[_num_ip_per_cell*73  + ip] = A[1][0][0][2];
      _w_tangent[_num_ip_per_cell*34  + ip] = _w_tangent[_num_ip_per_cell*74  + ip] = A[1][0][2][0]; 
      
      _w_tangent[_num_ip_per_cell*35  + ip] = A[1][2][1][2]; 
      _w_tangent[_num_ip_per_cell*36  + ip] = _w_tangent[_num_ip_per_cell*75  + ip] = A[1][2][2][1];
      _w_tangent[_num_ip_per_cell*37  + ip] = _w_tangent[_num_ip_per_cell*76  + ip] = A[1][2][0][2];
      _w_tangent[_num_ip_per_cell*38  + ip] = _w_tangent[_num_ip_per_cell*77  + ip] = A[1][2][2][0]; 
      
      _w_tangent[_num_ip_per_cell*39  + ip] = A[2][1][2][1];
      _w_tangent[_num_ip_per_cell*40  + ip] = _w_tangent[_num_ip_per_cell*78  + ip] = A[2][1][0][2];
      _w_tangent[_num_ip_per_cell*41  + ip] = _w_tangent[_num_ip_per_cell*79  + ip] = A[2][1][2][0];
      
      _w_tangent[_num_ip_per_cell*42  + ip] = A[0][2][0][2];
      _w_tangent[_num_ip_per_cell*43  + ip] = _w_tangent[_num_ip_per_cell*80  + ip] = A[0][2][2][0];
      
          
      _w_tangent[_num_ip_per_cell*44  + ip] = A[2][0][2][0]; 
    
    }
    
    else
    {
      _w_stress[_num_ip_per_cell*0  + ip] = Piola(0,0);
      _w_stress[_num_ip_per_cell*1  + ip] = Piola(0,1);
      _w_stress[_num_ip_per_cell*2  + ip] = Piola(1,0);
      _w_stress[_num_ip_per_cell*3  + ip] = Piola(1,1);


      _w_tangent[_num_ip_per_cell*0  + ip] =  A[0][0][0][0]; //Tan_mat_1111;
      _w_tangent[_num_ip_per_cell*1  + ip] =  A[0][0][1][1];//Tan_mat_1122;
      _w_tangent[_num_ip_per_cell*2  + ip] =  A[0][0][0][1];//Tan_mat_1112;
        
      _w_tangent[_num_ip_per_cell*3  + ip] =  A[1][1][0][0];//Tan_mat_2211;
      _w_tangent[_num_ip_per_cell*4  + ip] =  A[1][1][1][1];//Tan_mat_2222;
      _w_tangent[_num_ip_per_cell*5  + ip] =  A[1][1][0][1];//Tan_mat_2212;
        
      _w_tangent[_num_ip_per_cell*6  + ip] =  A[0][1][0][0];//Tan_mat_1211;
      _w_tangent[_num_ip_per_cell*7  + ip] =  A[0][1][1][1];//Tan_mat_1222;
      _w_tangent[_num_ip_per_cell*8  + ip] =  A[0][1][0][1];//Tan_mat_1212;
        
      _w_tangent[_num_ip_per_cell*9  + ip] =  A[0][0][1][0];//Tan_mat_1121;
      _w_tangent[_num_ip_per_cell*10 + ip] =  A[1][0][1][0];//Tan_mat_2121;
      _w_tangent[_num_ip_per_cell*11 + ip] =  A[1][0][0][1];//Tan_mat_2112;
      _w_tangent[_num_ip_per_cell*12 + ip] =  A[1][0][1][1];//Tan_mat_2122;
        
      _w_tangent[_num_ip_per_cell*13 + ip] =  A[1][0][0][0];//Tan_mat_2111; //2111
      _w_tangent[_num_ip_per_cell*14 + ip] =  A[0][1][1][0];//Tan_mat_1221; //1221
      _w_tangent[_num_ip_per_cell*15 + ip] =  A[1][1][1][0];//Tan_mat_2221; //2221
    }
  }

}


//-----------------------------------------------------------------------------
void ConstitutiveUpdate::update_history()
{
  // Update plastic elements
  const boost::multi_array<double, 3>& old_eps = _eps_p_equiv.old_data();
  const boost::multi_array<double, 3>& new_eps = _eps_p_equiv.current_data();

  const std::size_t num_cells = _plastic_last.shape()[0];
  const std::size_t ip_per_cell = _plastic_last.shape()[1];
  dolfin_assert(old_eps.shape()[0] == num_cells);

  for (std::size_t c = 0; c < num_cells; ++c)
  {
    for (std::size_t p = 0; p < ip_per_cell; ++p)
    {
      if ((new_eps[c][p][0] - old_eps[c][p][0] > 0.0))
        _plastic_last[c][p] = true;
      else
        _plastic_last[c][p] = false;
    }
  }

  _Fp_column.update_history();
  _eps_p_equiv.update_history();
}
//-----------------------------------------------------------------------------
